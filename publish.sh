#!/bin/bash

set -ex

PUBLIC=public/
WEBSITE=website/

# clean and re-populate the public/ folder
mkdir -p "$PUBLIC"
find "$PUBLIC" -type f \( -name "*.html" -or -name "*.gz" -or -name "*.svg"  -or -name "*.css" -or -name "*.pdf" \) -exec rm "{}" \;
cp -r "$WEBSITE"/* "$PUBLIC/"

# generate the logo
mkdir -p "$PUBLIC/icons"
cargo run --manifest-path logo-generator/Cargo.toml -- --color "{{logo-color}}" > "$PUBLIC/icons/logo.svg"

# replace color labels
patterns=('s/{{logo-color}}/#333/'
		  's/{{icon-color}}/#333/'
		  's/{{text-color}}/#333/'
		  's/{{url-color}}/#c21111/');

for pattern in ${patterns[@]}; do
   find "$PUBLIC" -type f -not -path "$PUBLIC/.git/*" -exec sed -i "$pattern" {} \;
done

# zip everything, but keep original
find "$PUBLIC" -type f -exec gzip -k "{}" \;

# download CVs from dedicated repo
mkdir -p "$PUBLIC/cv"
curl https://lobisquit.gitlab.io/cv/Enrico_Lovisotto_CV_en.pdf \
     --output "$PUBLIC/cv/EL_english.pdf"

curl https://lobisquit.gitlab.io/cv/Enrico_Lovisotto_CV_it.pdf \
     --output "$PUBLIC/cv/EL_italian.pdf"
