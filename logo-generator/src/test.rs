use crate::models::{Line, Point};

#[test]
fn test_intersecting_vertical() {
    let p1 = Point { x: 100.0, y: 1.0 };
    let p2 = Point { x: 100.0, y: 2.0 };

    let line = Line::intersecting(&p1, &p2);
    assert!(line.contains(&p1));
    assert!(line.contains(&p2));
}

#[test]
fn test_intersecting() {
    let p1 = Point { x: 20.0, y: 1.0 };
    let p2 = Point { x: 10.0, y: 2.0 };

    let line = Line::intersecting(&p1, &p2);
    println!("{:#?}", line);

    assert!(line.contains(&p1));
    assert!(line.contains(&p2));
}

#[test]
fn test_normalize() {
    let line = Line {
        a: 0.1,
        b: 0.2,
        c: 0.3,
    };
    let norm_line = line.normalize();

    println!("{:#?}", norm_line);

    assert_eq!(norm_line.a, 1.0);
    assert_eq!(norm_line.b, 2.0);
    assert_eq!(norm_line.c, 3.0);
}

#[test]
fn test_move_parallel_up() {
    let distance = 10.0;

    let p1 = Point { x: 20.0, y: 1.0 };
    let p2 = Point { x: 10.0, y: 2.0 };

    let line = Line::intersecting(&p1, &p2).normalize();
    let other_line = line.move_parallel_up(distance);

    let norm_line = line.normalize();

    println!("{:#?}", norm_line);

    assert_eq!(other_line.distance_p(&p1), distance);
    assert_eq!(other_line.distance_p(&p2), distance);
}

#[test]
fn test_move_parallel_right() {
    let distance = 10.0;

    let p1 = Point { x: 10.0, y: 1.0 };
    let p2 = Point { x: 10.0, y: 2.0 };

    let line = Line::intersecting(&p1, &p2);
    let other_line = line.move_parallel_up(distance);

    println!("{:#?}", line);
    println!("{:#?}", other_line);

    assert_eq!(other_line.distance_p(&p1), distance);
    assert_eq!(other_line.distance_p(&p2), distance);
}

#[test]
fn test_intersect() {
    let line = Line {
        a: 1.0,
        b: 1.0,
        c: 0.0,
    };
    let other_line = Line {
        a: 1.0,
        b: -10.0,
        c: 0.0,
    };

    println!("{:#?}", line);
    println!("{:#?}", other_line);

    let expected_point = Point { x: 0.0, y: 0.0 };
    assert_eq!(line.intersect(&other_line).unwrap(), expected_point);
}

#[test]
fn test_intersect_same_line() {
    let line = Line {
        a: 1.0,
        b: 1.0,
        c: 0.0,
    };
    let other_line = Line {
        a: 1.0,
        b: 1.0,
        c: 0.0,
    };

    println!("{:#?}", line);
    println!("{:#?}", other_line);

    let expected_point = Point { x: 0.0, y: 0.0 };
    assert_eq!(line.intersect(&other_line).unwrap(), expected_point);
}

#[test]
fn test_intersect_parallel_different_lines() {
    let line = Line {
        a: 1.0,
        b: 1.0,
        c: 0.0,
    };
    let other_line = Line {
        a: 1.0,
        b: 1.0,
        c: 0.0,
    };

    println!("{:#?}", line);
    println!("{:#?}", other_line);

    let expected_point = Point { x: 0.0, y: 0.0 };
    assert_eq!(line.intersect(&other_line).unwrap(), expected_point);
}

#[test]
fn test_intersect_with_horizontal() {
    let line = Line {
        a: 1.0,
        b: -1.0,
        c: 0.0,
    };
    let other_line = Line {
        a: 1.0,
        b: 0.0,
        c: -1.0,
    };

    println!("{:#?}", line);
    println!("{:#?}", other_line);

    let expected_point = Point { x: 1.0, y: 1.0 };
    assert_eq!(line.intersect(&other_line).unwrap(), expected_point);
}
