mod models;

#[cfg(test)]
mod test;

use crate::models::{Line, Point};
use clap::{App, Arg};

fn get_inner_triangle(thickness: f32, p1: &Point, p2: &Point, p3: &Point) -> [Point; 3] {
    // left inner triangle
    let line1_2 = Line::intersecting(&p1, &p2);
    let sign = line1_2.evaluate(&p3).signum();

    let inner_line1_2 = line1_2.move_parallel_up(-sign * thickness);

    let line1_3 = Line::intersecting(&p1, &p3);
    let sign = line1_3.evaluate(&p2).signum();
    let inner_line1_3 = line1_3.move_parallel_up(-sign * thickness);

    let line2_3 = Line::intersecting(&p2, &p3);
    let sign = line2_3.evaluate(&p1).signum();
    let inner_line2_3 = line2_3.move_parallel_up(-sign * thickness);

    let p4 = inner_line1_2.intersect(&inner_line1_3).unwrap();
    let p5 = inner_line1_2.intersect(&inner_line2_3).unwrap();
    let p6 = inner_line1_3.intersect(&inner_line2_3).unwrap();

    [p4, p5, p6]
}

fn format_points(points: Vec<&Point>) -> String {
    points
        .iter()
        .map(|p| format!("{},{}", p.x, p.y))
        .collect::<Vec<String>>()
        .join(" ")
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("Logo generator")
        .arg(Arg::with_name("height").long("height").takes_value(true))
        .arg(Arg::with_name("width").long("width").takes_value(true))
        .arg(
            Arg::with_name("thickness")
                .long("thickness")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("distance")
                .long("distance")
                .takes_value(true),
        )
        .arg(Arg::with_name("color").long("color").takes_value(true))
        .get_matches();

    let height: f32 = matches.value_of("height").unwrap_or("15.0").parse()?;
    let width: f32 = matches.value_of("width").unwrap_or("10.0").parse()?;
    let thickness: f32 = matches.value_of("thickness").unwrap_or("1.0").parse()?;
    let distance: f32 = matches.value_of("distance").unwrap_or("2.0").parse()?;
    let color = matches.value_of("color").unwrap_or("black");

    // left triangle
    let left_p1 = Point { x: 0.0, y: 0.0 };
    let left_p2 = Point { x: width, y: 0.0 };
    let left_p3 = Point {
        x: width / 2.0,
        y: height,
    };

    let [left_p4, left_p5, left_p6] = get_inner_triangle(thickness, &left_p1, &left_p2, &left_p3);

    // print an SVG with the two shapes
    let left_outer = format_points(vec![&left_p1, &left_p2, &left_p3]);
    let left_inner = format_points(vec![&left_p4, &left_p6, &left_p5]);

    // right triangle
    let left_line2_3 = Line::intersecting(&left_p2, &left_p3);
    let right_line2_3 = left_line2_3.move_parallel_up(distance);

    let left_line1_2 = Line::intersecting(&left_p1, &left_p2);
    let p = left_line1_2.intersect(&right_line2_3).unwrap();
    let Point { x: x_prime, .. } = p;

    // map the first points reversing them and translating of x_prime
    let right_p1 = Point {
        x: left_p1.x + x_prime - width / 2.0,
        y: height - left_p1.y,
    };

    let right_p2 = Point {
        x: left_p2.x + x_prime - width / 2.0,
        y: height - left_p2.y,
    };

    let right_p3 = Point {
        x: left_p3.x + x_prime - width / 2.0,
        y: height - left_p3.y,
    };

    let [right_p4, right_p5, right_p6] =
        get_inner_triangle(thickness, &right_p1, &right_p2, &right_p3);

    let right_outer = format_points(vec![&right_p1, &right_p2, &right_p3]);
    let right_inner = format_points(vec![&right_p5, &right_p6, &right_p4]);

    println!(
        r#"<?xml version="1.0" ?>
<svg width="{width}" height="{height}" viewBox="0 0 {width} {height}" xmlns="http://www.w3.org/2000/svg">
  <defs>
    <mask id="left_hole">
      <polygon points="{left_outer}" fill="white"/>
      <polygon points="{left_inner}" fill="black"/>
    </mask>

    <mask id="right_hole">
      <polygon points="{right_outer}" fill="white"/>
      <polygon points="{right_inner}" fill="black"/>
    </mask>
  </defs>

  <polygon points="{left_outer}"
	   fill="{color}"
	   mask="url(#left_hole)"/>

  <polygon points="{right_outer}"
	   fill="{color}"
	   mask="url(#right_hole)"/>
</svg>
"#,
        width = x_prime + width / 2.0,
        height = height,
        left_outer = left_outer,
        left_inner = left_inner,
        right_outer = right_outer,
        right_inner = right_inner,
        color = color
    );

    Ok(())
}
