#[derive(Debug, PartialEq)]
pub struct Point {
    pub x: f32,
    pub y: f32,
}

#[derive(Debug, PartialEq)]
pub struct Line {
    pub a: f32,
    pub b: f32,
    pub c: f32,
}

impl Line {
    pub fn evaluate(&self, p: &Point) -> f32 {
        self.a * p.x + self.b * p.y + self.c
    }

    pub fn contains(&self, p: &Point) -> bool {
        self.evaluate(p) == 0.0
    }

    pub fn intersecting(p1: &Point, p2: &Point) -> Line {
        if p1.x == p2.x {
            // vertical line
            Line {
                a: 1.0,
                b: 0.0,
                c: -p1.x,
            }
        } else {
            let m = (p2.y - p1.y) / (p2.x - p1.x);
            let q = p1.y - m * p1.x;

            Line {
                a: m,
                b: -1.0,
                c: q,
            }
        }
    }

    pub fn intersect(&self, other: &Line) -> Option<Point> {
        let x_num = other.c * self.b - self.c * other.b;
        let x_den = other.b * self.a - self.b * other.a;

        if x_den == 0.0 {
            // two lines are parallel
            if self.c == other.c {
                // same row: any point is an intersection
                Some(Point { x: 0.0, y: self.c })
            } else {
                // no intersection
                None
            }
        } else {
            let x = x_num / x_den;

            let y = if self.b != 0.0 {
                -(self.a * x + self.c) / self.b
            } else {
                -(other.a * x + other.c) / other.b
            };

            Some(Point { x: x, y: y })
        }
    }

    pub fn normalize(self) -> Line {
        if self.a == 0.0 && self.b == 0.0 {
            panic!("Ill-defined line: {:#?}", self);
        } else {
            // normalize using either of the two leading factors
            let factor = if self.a == 0.0 { self.b } else { self.a };

            Line {
                a: self.a / factor,
                b: self.b / factor,
                c: self.c / factor,
            }
        }
    }

    pub fn parallel_to(&self, other: &Line) -> bool {
        return (self.a == 0.0 && other.a == 0.0) || (self.a * other.b == self.b * other.a);
    }

    pub fn move_parallel_up(&self, distance: f32) -> Line {
        if self.b == 0.0 {
            Line {
                a: self.a,
                b: self.b,
                c: self.c + distance,
            }
        } else {
            // normalize
            let m = -self.a / self.b;
            let q = -self.c / self.b;

            let delta_q = distance * (1.0 + m * m).sqrt();

            Line {
                a: m,
                b: -1.0,
                c: q + delta_q,
            }
        }
    }

    pub fn distance_p(&self, p: &Point) -> f32 {
        let num = (self.a * p.x + self.b * p.y + self.c).abs();
        let den = (self.a * self.a + self.b * self.b).sqrt();
        num / den
    }
}
